import subprocess

inputS = ["first_word", "", "dfsfsfsfsfs", "third_word", "r" * 256, "two words key"]
outputS = ["", "third word explanation", "",  "first word explanation", "", "two words key explanation"]
errorS = ["", "Error: key not found", "Error: key not found", "", "Error: buffer overflow", ""]
exitCodes = [0, 1, 1, 0, 1, 0]

for i in range(len(inputS)):
    p = subprocess.Popen(["./main"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = p.communicate(input=inputS[i].encode())
    Sout,Serr = stdout.decode().strip(),stderr.decode().strip()
    exitCode = p.returncode
    if Sout == outputS[i] and Serr == errorS[i] and exitCode == exitCodes[i]:
        print("Test "+str(i+1)+" passed")
    else:
        print(f"Test {i+1} failed. Result {{Sout: '{Sout}', Serr: '{Serr}', exitCode: {exitCode}}}, expected: {{Sout: '{outputS[i]}', Serr: '{errorS[i]}', exitCode: {exitCodes[i]}}}")
