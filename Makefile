ASM=nasm
ASM_FLAGS=-felf64
LD=ld
PYTHON=python

all: clean main

main: main.o dict.o lib.o
	$(LD) -o $@ $^

%.o: %.asm
	$(ASM) $(ASM_FLAGS) -o $@ $<

main.o: main.asm lib.inc dict.inc colon.inc

dict.asm: dict.asm lib.inc

clean:
	rm -rf *.o main

run: all
	./main && echo "OK!" || echo "FAIL!"

test: all
	$(PYTHON) test.py

.PHONY: clean all run test
