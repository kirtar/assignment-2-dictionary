%define next_el 0

%macro colon 2
    %ifnstr %1
        %fatal "Error: first argument is not a string "
    %endif

    %ifnid %2
        %fatal "Error: second argument is not a label"
    %endif

    %2:
        dq next_el
        db %1, 0
        %define next_el %2

%endmacro
