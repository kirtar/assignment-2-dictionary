global _start

%include "lib.inc"
%include "dict.inc"
%include "words.inc"

%define buffer_size 256
%define exit_err_code 1
%define pointer_size 8

section .bss
    buffer: resb buffer_size

section .rodata
    err_buffer_of: db "Error: buffer overflow",0
    err_key_not_found: db "Error: key not found",0
    err_read: db "Error: read error",0

    
section .text


_start:
    xor rax,rax
    xor rdi,rdi
    lea rsi, [buffer]
    mov rdx, buffer_size
    syscall
    cmp rax, 0
    jl .read_error
    cmp rax, buffer_size
    jae .buffer_overflow
    mov rdi, buffer
	
    call find_word
    test rax,rax
    jz .key_not_found
	
    mov rdi,rax
    add rdi,pointer_size
    push rdi
    call string_length
    pop rdi
    add rdi,rax
    add rdi,1
    call print_string
    call print_newline
    xor rdi,rdi
    call exit

.read_error:
    mov rdi, err_read
    jmp .err_exit

.buffer_overflow:
    mov rdi,err_buffer_of
    jmp .err_exit
	
.key_not_found:
    mov rdi,err_key_not_found
.err_exit:
    call print_error
    call print_newline
    mov rdi, exit_err_code
    call exit





