global exit
global string_length
global print_string
global print_error ; new
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy


section .text

%define SYS_EXIT 60



; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, SYS_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax,rax
    .loop:
        cmp byte[rdi+rax],0
        je .done
        inc rax
        jmp .loop
    .done:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi
    mov rdx, rax 
    mov rsi, rdi 
    mov rax, 1 
    mov rdi, 1 
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi            
    mov rax, 1          
    mov rdi, 1          
    mov rsi, rsp        
    mov rdx, 1          
    syscall
    pop rdi             

    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, `\n`
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov r8, rsp
    mov r9, 10
    push 0

    .loop:
        xor rdx, rdx
        div r9
        add rdx, '0'
        dec rsp
        mov byte[rsp], dl
        test rax, rax
        ja .loop
        mov rdi, rsp
        push r8
        call print_string
        pop r8
        mov rsp, r8
        ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jge .print_abs
    neg rdi
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    .print_abs:
        call print_uint 
        ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals: 
    xor rax, rax            
    xor rcx, rcx            
    .loop:
        mov al, [rdi + rcx]     
        cmp al, [rsi + rcx]     
        jne .fail               
        cmp al, 0   
        je .success            
        inc rcx                 
        jmp .loop

    .success:
        mov rax, 1              
        ret
    .fail:
        xor rax, rax            
        ret
        

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    mov rax, 0
    mov rdi, 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор


read_word:
    xor rcx, rcx

    .loop:
        push rdi
        push rcx
        push rsi
        call read_char
        pop rsi
        pop rcx
        pop rdi

        test rax,rax
        jz .break

        cmp rax, ' '
        jz .space

        cmp rax, `\t`
        jz .space

        cmp rax, `\n`
        jz .space

        cmp rcx, rsi
        jge .fail

        mov [rdi+rcx], rax
        inc rcx

        jmp .loop

    .fail:
            xor rax, rax
            xor rdx, rdx
            ret

    .space:
            test rcx,rcx
            jz .loop
            jmp .break

    .break:
            mov byte[rdi+rcx], 0
            mov rax, rdi
            mov rdx, rcx
            ret

 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax       
    xor rdx, rdx       
    xor rcx, rcx      

.loop:
    mov dl, byte [rdi+rcx]  
    sub dl, '0'             

    cmp dl, 9              
    ja .end                

    imul rax, rax, 10       
    add rax, rdx            
    inc rcx                 
    jmp .loop               

.end:
    mov rdx, rcx            
    ret                     





; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
     mov al, byte[rdi]
     cmp al, '-'
     je .negative
     call parse_uint
     jmp .end
     .negative:
          inc rdi
          call parse_uint
          inc rdx
          neg rax
          ret
     .end:
         ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	xor rax, rax
.loop:
	cmp rax, rdx
	jge .failure
	mov cl, byte[rdi + rax]
	mov byte[rsi + rax], cl
	inc rax
	test cl, cl
	je .end
	jmp .loop
.failure:	
	xor rax, rax
.end:
	ret 


print_error:
	push rdi
	call string_length
	mov rsi,rdi
	mov rdx, rax
	mov rdi,2
	mov rax,1
	syscall
    ret
